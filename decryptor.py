from urllib.request import urlopen

import base64
import argparse

from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA

parser = argparse.ArgumentParser()
parser.add_argument('url', help='url or pastebin hash')
args = parser.parse_args()

if args.url.startswith('http'):
    url = args.url
else:
    url = 'http://pastebin.com/raw/{}'.format(args.url)

encrypted = urlopen(url).read()
priv_key = b''

rsakey = RSA.importKey(priv_key)
cipher = PKCS1_OAEP.new(rsakey)

chunk_size = 256
offset = 0
decrypted = b''
decoded = base64.b64decode(encrypted)
while offset < len(decoded):
    decrypted += cipher.decrypt(decoded[offset:offset + chunk_size])
    offset += chunk_size

print(decrypted)
