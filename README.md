# Playground

Simply python trojan to play around with some ideas. Uses:
- pastebin to fetch config and publish data
- it can be scalled up via new modules published on bitbucket

THIS IS JUST A PLAYGROUND FOR PYTHON-FUN. I AMO NOT TAKING ANY RESPONSIBILITY FOR USING BY 3RD PARTIES!

## Usage

Copy settings.py.template and rename to settings.py and populate with data.
Your pastebin config should looks like this:

```
    [
      {
        "module": "sysinfo"
      }
    ]
```
where module is module name from modules/ dir. During communication trojan can fetch new modules remotely basing on module name.

## TODO
- self destruct module
- refresher
- keylogger
- binary form maybe

## Copyright

Copyright (c) 2016 Pawel Buchowski.
